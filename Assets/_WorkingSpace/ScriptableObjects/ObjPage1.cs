using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ObjectPage1")]
public class ObjPage1 : ScriptableObject {
    public Sprite headerSprite_TH;
    public List<ButtonWithKey> buttonWithKeys;
}

[System.Serializable]
public struct ButtonWithKey {
    public List<Sprite> BTN;
    public string BTN_Key;
}