using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ObjectPage3")]
public class ObjPage3 : ScriptableObject {
    public Sprite headerSprite_TH;
    public ButtonImageOnly[] buttonImageOnly;
}

[System.Serializable]
public struct ButtonImageOnly {
    public Sprite BTN_TH;
}
