using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ObjectPage2")]
public class ObjPage2 : ScriptableObject {
    public Sprite headerSprite_TH;
    public ButtonWithPressAndKey[] ButtonWithPressAndKeys;
}

[System.Serializable]
public struct ButtonWithPressAndKey
{
    public Sprite BTN_TH;
    public Sprite BTN_TH_press;
    public string BTN_Key;
}
