using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;

public class DataPageMainControl : MonoBehaviour {
    public DataPageManager pageManager;
    public Image headerImg;
    public ImageGrouping headerGroup;
    public Image[] buttonImages;
    public ImageGrouping[] buttonGroups;
    public TextSender tSender;

    List<Button> buttonGroup;
    float delayButton = .3f;

    void Start() {
        buttonGroup = new List<Button>();
        headerImg.sprite = headerGroup.imgData[Global.ins.GetLanguageID()];

        for (int i = 0; i < buttonImages.Length; i++) {
            buttonImages[i].sprite = buttonGroups[i].imgData[Global.ins.GetLanguageID()];
            buttonGroup.Add(buttonImages[i].GetComponent<Button>());
        }
        
        foreach (Button b in buttonGroup) {
            b.enabled = false;
            b.image.color = new Color(b.image.color.r, b.image.color.g, b.image.color.b, 0);
            b.image.DOFade(1f, .3f).SetDelay(1.5f).OnComplete(() => b.enabled = true);
        }

    }

    public void OnButtonPress(int id) {
        pageManager.FadeHomeButton();
        pageManager.EnablePanelByIndex(id);
    }

    private void OnEnable() {
        if(tSender) tSender.SendString();

        if (buttonGroup!=null) {
            foreach(Button b in buttonGroup) {
                b.enabled = false;
                b.image.color = new Color(b.image.color.r, b.image.color.g, b.image.color.b, 0);
                b.image.DOFade(1f, .3f).SetDelay(delayButton).OnComplete(() => b.enabled = true);
            }
        }
    }
}
