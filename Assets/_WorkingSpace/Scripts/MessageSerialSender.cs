using System;
using UnityEngine;
using UnityEngine.UI;

public class MessageSerialSender : MonoBehaviour {
    public Image[] imageEnableButtons;
    public float delayMsgSndBtn;
    float lastMsgSndPress;
    // Start is called before the first frame update
    void Start() {
        foreach(Image img in imageEnableButtons) {
            img.gameObject.SetActive(false);
        }
    }

    public void OnMessageSender(int id) {
        if (Time.unscaledTime < lastMsgSndPress + delayMsgSndBtn) return;

        SetTextureButtonActive(id);

        string s = "A";
        switch (id) {
            case 0:
                s = "A";
                break;
            case 1:
                s = "B";
                break;
            case 2:
                s = "C";
                break;
        }

        lastMsgSndPress = Time.unscaledTime;
//        Global.ins.SerialMsgSndr(s);
    }

    private void SetTextureButtonActive(int id) {
        for(int i = 0; i < imageEnableButtons.Length; i++) {
            if(i == id) {
                imageEnableButtons[i].gameObject.SetActive(true);
            } else {
                imageEnableButtons[i].gameObject.SetActive(false);
            }
        }
    }

}
