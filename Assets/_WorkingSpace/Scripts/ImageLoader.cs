using System.IO;
using UnityEngine;

public class ImageLoader : MonoBehaviour{
    public static Texture2D LoadT2D(string filePath) {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath)) { 
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);

            return tex;
        } else {
            return null;
        }
    }

    public static Sprite SpriteFromT2D(Texture2D tex) {
        if (tex != null) {
            return Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        } else {
            return null;
        }
    }
   
}
