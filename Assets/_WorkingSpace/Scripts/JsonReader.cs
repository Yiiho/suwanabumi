using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.IO;

public class JsonReader : MonoBehaviour {
    public delegate void ReadJson();
    public event ReadJson OnJsonReaded;
    public string jsonFileName;

    public List<JsonGroupData> listGroupData = new List<JsonGroupData>(); 
    public float intervalTimer { get; private set; }
    public string ip { get; private set; }
    public int port { get; private set; }
    public void ReadAllData() {
        string dir = Application.dataPath + "/../" + jsonFileName;
        LoadJsonFile(dir);
    }

    private void LoadJsonFile(string path) {
        if (File.Exists(path)) {
            string s = File.ReadAllText(path);
            DoSimpleJson(s);
        } else {
            Debug.Log("File Not Exists at "+path);
        }
    }

    private void DoSimpleJson(string s) {
        string dir = Application.dataPath + "/../";
        //Read JSON.
        JSONNode jsonNode = SimpleJSON.JSON.Parse(s);
        //Add fixed parameter struture.
        intervalTimer = float.Parse(SplitString(jsonNode["IntervalTime"].ToString()));
        ip = SplitString(jsonNode["IP"].ToString());
        port = int.Parse(SplitString(jsonNode["Port"].ToString()));

        for (int i = 0; i < jsonNode["Items"].Count; i++) {
            List<string> dumList = new List<string>();
            JsonGroupData gData = new JsonGroupData();
            //Add struct database.
            gData.itemName = SplitString(jsonNode["Items"][i]["name"].ToString());
            gData.keys = new string[jsonNode["Items"][i]["keys"].Count]; //Create length of array.
            for(int j = 0; j < jsonNode["Items"][i]["keys"].Count; j++) {
                gData.keys[j] = jsonNode["Items"][i]["keys"][j];
            }
            listGroupData.Add(gData);           
        }
        //Json read completed then dispatch here.
        if (OnJsonReaded!=null) OnJsonReaded();
    }

    string SplitString(string s) {
        if(s== "\"\"") {  //reader get "" from json.
            s = "";
        } else { //reader have data not "".
            s = s.Replace(@"\", "/");
            s = s.Replace("\"", "");
        }
        return s;
    }

}

[System.Serializable]
public struct JsonGroupData {
    public string itemName;
    public string[] keys;
}
