//////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Button auto toggle will shown a disable image object or etc in child.
/// Throw this class to parent object.
/// This class will be get all button component in child and push in allButton[].
/// Then it will create event to trrigger show or hide when button clicked.
/// Also it will toggle child at index 0. (Make sure all object you want to show is in child 0)
///
//////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using UnityEngine;
using UnityEngine.UI;

public class ButtonImageToggleAuto : MonoBehaviour {
    [Tooltip("Enable Object index. If not have any conclusion then set to -1.")]
    public int defaultActiveObjectIndex = -1;
    Button[] allButton;
    GameObject[] childOfButtons;

    void Start() {
        allButton = GetComponentsInChildren<Button>();
        childOfButtons = new GameObject[allButton.Length];
        GameObject go;
        for (int i= 0; i < allButton.Length; i++) {
            int _i = i;
            allButton[i].onClick.AddListener(delegate { ToggleButton(_i); });
            try {
                go = allButton[i].transform.GetChild(0).gameObject;
            } catch (Exception e) {
                go = null;
            }
            childOfButtons[i] = go;
        }
        ResetToDefault();
    }

  
    public void ToggleButton(int id) {
        bool isActive;
        for (int i = 0; i < childOfButtons.Length; i++) {
            isActive = (i == id) ? true : false;
            if (childOfButtons[i] != null) childOfButtons[i].SetActive(isActive);
        }
    }

    public void ResetToDefault() {
        ToggleButton(defaultActiveObjectIndex);
    }

    private void OnDisable() {
        ResetToDefault();
    }
}
