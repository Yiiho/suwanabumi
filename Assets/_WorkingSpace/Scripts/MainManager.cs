using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MainManager : MonoBehaviour {
    public Button[] navButtons;
    public ButtonReplaceImage[] imgButtons;
    public NavGroup[] navObjs;
    public float delayForFirstObj = 2.15f;
    public float delayPressBtn;
    public float delayDisableObj = 0.3f;
    float lastPressTime = -10;
    int currentSelectIndex = -1; //set -1 to set initial scene.

    public float intervalTimer = 50f;

    public SocketMessageManager messenger;

    void Start() {
        foreach (NavGroup _nav in navObjs) { //Disable all nav gamobject.
            _nav.navObjEnabler.SetActive(false);
        }
        DOVirtual.DelayedCall(delayForFirstObj, () => OnSelectScene(0) ); //Init after NavBar animate completed.

        if (messenger) messenger.OnMessageRecieved += OnReceievedData;
    }

    private void OnReceievedData(string str) {
       //to do message.

    }

    public void SendDataToMessenger(string str) {
        messenger.SendData(str);
    }

    public void OnSelectScene(int _index) {
        if (_index == currentSelectIndex) return;
        if (Time.unscaledTime < lastPressTime + delayPressBtn) return; //Brake user to spam button.

        lastPressTime = Time.unscaledTime;
        SetTextureNavButton(_index);
        if (currentSelectIndex == -1) {
            OnLoadNavObj(_index);
        } else {
            int _tmbCurrentIndex = currentSelectIndex;
            navObjs[_tmbCurrentIndex].navObjDisabler.SetActive(true); //Active disable's timeline.
            DOVirtual.DelayedCall(delayDisableObj, () => {
                //Load new object.
                OnLoadNavObj(_index);
                //Disable old object.
                navObjs[_tmbCurrentIndex].navObjEnabler.SetActive(false);
                navObjs[_tmbCurrentIndex].navObjDisabler.SetActive(false);
            });
        }
        currentSelectIndex = _index;
    }

    private void OnLoadNavObj(int _index) {
        navObjs[_index].navObjEnabler.SetActive(true);
    }

    private void SetTextureNavButton(int _index) {
        //Set active button with anim and chage active texture.
        navButtons[_index].transform.DOScale(1.3f, .25f).OnComplete(() => { 
            navButtons[_index].image.sprite = imgButtons[_index].buttonActive;
            navButtons[_index].transform.DOScale(1f, .25f);
        });
        //Set deactive button to default texture.
        for (int i = 0; i < navButtons.Length; i++) { 
            if(i != _index) {
                navButtons[i].image.sprite = imgButtons[i].buttonDeactive;
            }
        }
    }

}

[System.Serializable]
public struct NavGroup {
    public GameObject navObjEnabler;
    public GameObject navObjDisabler;
}

[System.Serializable]
public struct ButtonReplaceImage {
    public Sprite buttonActive;
    public Sprite buttonDeactive;
}