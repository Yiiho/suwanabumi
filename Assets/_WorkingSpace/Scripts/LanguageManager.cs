using UnityEngine;
using DG.Tweening;

public class LanguageManager : MonoBehaviour {
    public GameObject thisObj;
    public GameObject nextObj;

    void Start() {
        Global.ins.IsStillUpdateInterval = true; 
    }

    public void SelectLanguage(int id) {
        switch (id) {
            case 0:
                Global.ins.CurrentLanguage = Global.lang.TH;
                break;
            case 1:
                Global.ins.CurrentLanguage = Global.lang.EN;
                break;
            case 2:
                Global.ins.CurrentLanguage = Global.lang.CN;
                break;
        }

        thisObj.transform.DOMoveY(2000, 1).OnComplete(() => thisObj.SetActive(false));
        nextObj.SetActive(true);

    }

}
