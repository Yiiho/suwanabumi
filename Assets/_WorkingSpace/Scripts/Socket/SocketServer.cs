using UnityEngine;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System;
using System.Text;
using UnityEngine.UI;
using System.Collections;

public class SocketServer : SocketBase {
    Thread SocketThread;
    volatile bool keepReading = false;

    // Use this for initialization
    void Start() {
        Application.runInBackground = true;
        if (connectAfterAwake) startServer();

        if(extension || messenger) StartCoroutine(UpdateLog());
    }
    /*
    private void Update() {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            TryDestroyAndReconnect();
        }
    }
    */
    void TryDestroyAndReconnect() {
        stopServer();

        Invoke("startServer", 5);
    }

    bool isRoundUpdateLog = false;
    string tmbMsgSndr = "";
    IEnumerator UpdateLog() {
        if(tmbMsgSndr != MsgSndr && MsgSndr != "") {
            tmbMsgSndr = MsgSndr;
            SendMessage(tmbMsgSndr);
            MsgSndr = "";
        }
        if (isRoundUpdateLog) {
            if (logs != "") {
                string s = logs;
                if (extension) extension.AppendLog(s);
                if (messenger) messenger.RecieveData(s);
                logs = "";
            }
        }
        isRoundUpdateLog = !isRoundUpdateLog; //log will be update per 0.1 second.
        yield return new WaitForSeconds(.05f);
        StartCoroutine(UpdateLog());
    }

    private void SendMessage(String str) {
        
        if (SocketThread == null) { //If connection is problem or missing.
            Debug.Log("SocketConnection problem! Try to reconnecting...");
            return;
        }

        byte[] byteSndr = Encoding.ASCII.GetBytes(str);
        Debug.Log("Send to Client: "+ str);
        handler.Send(byteSndr);
        
    }


    public void startServer() {
        SocketThread = new Thread(networkCode);
        SocketThread.IsBackground = true;
        SocketThread.Start();
    }

    private string GetIPAddress() {
        IPHostEntry host;
        string localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList) {
            if (ip.AddressFamily == AddressFamily.InterNetwork) {
                localIP = ip.ToString();
            }

        }
        return localIP;
    }

    Socket listener;
    Socket handler;

    void networkCode() {
        string data;

        // Data buffer for incoming data.
        byte[] bytes = new Byte[1024];

        // host running the application.
      //  Debug.Log("Ip " + getIPAddress().ToString());
      //  IPAddress[] ipArray = Dns.GetHostAddresses(getIPAddress());
      //  IPEndPoint localEndPoint = new IPEndPoint(ipArray[0], 1755);

        IPAddress[] ipArray = Dns.GetHostAddresses(ipAddress);
        Debug.Log("IP : " + ipArray[0].ToString());
        AddLog("IP : " + ipArray[0].ToString());
        IPEndPoint localEndPoint = new IPEndPoint(ipArray[0], port);
        Debug.Log("Port : " + port);
        AddLog("Port : " + port);

        // Create a TCP/IP socket.
        listener = new Socket(ipArray[0].AddressFamily,
            SocketType.Stream, ProtocolType.Tcp);

        // Bind the socket to the local endpoint and 
        // listen for incoming connections.

        try {
            listener.Bind(localEndPoint);
            listener.Listen(10);

            // Start listening for connections.
            while (true) {
                keepReading = true;

                // Program is suspended while waiting for an incoming connection.
                Debug.Log("Waiting for Connection");     //It works
                AddLog("Waiting for Connection");

                handler = listener.Accept();
                Debug.Log("Client Connected");     //It doesn't work.
                AddLog("Client Connected");
                data = null;

                // An incoming connection needs to be processed.
                while (keepReading) {
                    data = "";
                    bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);
                    Debug.Log("Received from Client :");
                    AddLog("Received from Client :");

                    if (bytesRec <= 0) {
                        keepReading = false;
                        handler.Disconnect(true);
                        break;
                    }

                    data = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    Debug.Log(data);
                    AddLog(data);

                    if (data.IndexOf("<EOF>") > -1) {
                        break;
                    }

                    System.Threading.Thread.Sleep(1);
                }

                System.Threading.Thread.Sleep(1);
            }
        } catch (Exception e) {
            Debug.Log(e.ToString());
        }
    }

    void stopServer() {
        keepReading = false;
        AddLog("Stop server!");

        //stop thread
        if (SocketThread != null) {
            SocketThread.Abort();
        }

        if (handler != null && handler.Connected) {
            handler.Disconnect(false);
            Debug.Log("Disconnected!");
            AddLog("Disconnected!");
        }
    }
    void AddLog(string str) {
        logs = str;
        Debug.Log(str);
    }
}
