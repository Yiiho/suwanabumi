using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SocketExtension : MonoBehaviour {
    public Text debugText;
    public string fileSaveFolderName;

    string logFilePath;
    StreamWriter writer;

    void Start() {
        logFilePath = Application.dataPath + "/../" + 
            fileSaveFolderName+"/"+createLogName()+".txt";
        CreateLogFile("[Logs]");

        debugText.gameObject.SetActive(false);
    }

    public void AppendLog(string str) {
        string _str = str;
        debugText.text += "\n" + _str;
        CreateLogFile(str);
    }

    void CreateLogFile(string str) {
        if (!File.Exists(logFilePath)) { //Create header content do not attach time.
            print(str + " not have log file");
            writer = new StreamWriter(logFilePath);
            writer.WriteLine(str);
        } else {
            print(str + " haved log file");
            string timestamp = "[" + DateTime.Now.ToString("HH:mm:ss:ff") + "] : ";
            writer = new StreamWriter(logFilePath, append: true);
            writer.WriteLine(timestamp + str);
        }
        writer.Flush();
        writer.Close();
        writer = null;
    }

    string createLogName() {
        return DateTime.Now.ToString("yyyyMMdd");
    }

    private void Update() {
        if (Input.GetKeyUp(KeyCode.M)) {
            debugText.text = "";
            debugText.gameObject.SetActive(!debugText.gameObject.activeSelf);
        }
    }

}
