using UnityEngine;
using DG.Tweening;

public class TextSender : MonoBehaviour {
    public string stringSender;
    float delaySend = .05f;
    SocketServerClient socket;

    void Start() {
        socket = Global.ins.socket;
    }

    public void SendString(string s="") {
        s = (s == "") ? stringSender: s;
        DOVirtual.DelayedCall(delaySend, ()=> socket.SendData(s));
    }

}
