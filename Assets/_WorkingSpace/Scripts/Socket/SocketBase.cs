using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocketBase : MonoBehaviour {
    [Tooltip("Connection url and port.")]
    public string ipAddress;
    public int port;
    [Tooltip("Auto bind server after this thread is active.")]
    public bool connectAfterAwake;
    [Tooltip("Extension for update log.")]
    public SocketExtension extension;
    [Tooltip("Extension for send or recieve message for other class.")]
    public SocketMessageManager messenger;

    protected string logs = "";

    /// <summary>
    /// Message wait to send.
    /// </summary>
    public string MsgSndr { get; set; }
    public string MsgRcvr { get; set; }

}
