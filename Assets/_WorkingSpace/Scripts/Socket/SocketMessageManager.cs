using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocketMessageManager : MonoBehaviour
{
    public delegate void MessageRecieved(string str);
    public MessageRecieved OnMessageRecieved;

    public SocketBase socketTarget;

    void Start()
    {
        
    }

    public void SendData(string str) {
        socketTarget.MsgSndr = str;
    }

    public void RecieveData(string str) {
        if (OnMessageRecieved != null) OnMessageRecieved(str);
    }

}
