using System;
using System.Collections;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

public class SocketServerClient : SocketBase {
	[Tooltip("Delay for check auto reconnect.(second)")]
	[Range(5,10)]
	public float updateTimer=5.0f;

	private TcpClient socketConnection;
	private Thread clientReceiveThread;

	void Start() {
		Application.runInBackground = true;
		if (connectAfterAwake) StartConnectToServer();

		if (extension) StartCoroutine(UpdateLog()); //Update log when have extension only.
	}
	IEnumerator UpdateConnectStatus() {
		yield return new WaitForSeconds(updateTimer);
		if (socketConnection == null || !socketConnection.Connected) {
			ReconnectServer(updateTimer*1.2f);
        } else {
			StartCoroutine(UpdateConnectStatus());
		}
	}

    private void ReconnectServer(float timer) {
		StopClient();
		Invoke("AddDelayToRecon", timer);
    }

	void AddDelayToRecon() {
		connectToServer();
		StartCoroutine(UpdateConnectStatus());
	}


	bool isRoundUpdateLog = false;
	string tmbMsgSndr = "";
	IEnumerator UpdateLog() {
		if (tmbMsgSndr != MsgSndr && MsgSndr != "") {
			tmbMsgSndr = MsgSndr;
			SendMessage(tmbMsgSndr);
			MsgSndr = "";
		}
		if (isRoundUpdateLog) {
			if (logs != "") {
				string s = logs;
				if (extension) extension.AppendLog(s);
				if (messenger) messenger.RecieveData(s);
				logs = "";
			}
		}
		isRoundUpdateLog = !isRoundUpdateLog; //log will be update per 0.1 second.
		yield return new WaitForSeconds(.05f);
		StartCoroutine(UpdateLog());
	}
	string lastSent = "";
	public void SendData(string str) {
		if (str == lastSent) return;
		SendMessage(str);
		lastSent = str;
    }
	

	/// <summary> 	
	/// Setup socket connection. 
	/// If want to trigger from other class. Uncheck connectAfterAwake and use this method to connect.
	/// </summary> 	
	public void StartConnectToServer(string _address=null, int _port=-1) {
		//Set default ip and port. (use for reconnection)
		ipAddress = (_address != null) ? _address: ipAddress;
		port = (_port != -1) ? _port: port;

		connectToServer();
		StartCoroutine(UpdateConnectStatus());
	}

    private void connectToServer() {
		clientReceiveThread = new Thread(ListenForData);
		clientReceiveThread.IsBackground = true;
		clientReceiveThread.Start();
	}

	/// <summary> 	
	/// Runs in background clientReceiveThread; Listens for incomming data. 	
	/// </summary>     
	private void ListenForData() {
		try {
			socketConnection = new TcpClient(ipAddress, port);
			Byte[] bytes = new Byte[1024];

			while (socketConnection.Connected) {
				// Get a stream object for reading 				
				using (NetworkStream stream = socketConnection.GetStream()) {
					Debug.Log("Connection status : OK");
					int length;
					// Read incomming stream into byte arrary. 					
					while ((length = stream.Read(bytes, 0, bytes.Length)) != 0) {
						var incommingData = new byte[length];
						Array.Copy(bytes, 0, incommingData, 0, length);
						// Convert byte array to string message. 						
						string serverMessage = Encoding.ASCII.GetString(incommingData);
						Debug.Log("server message received as: " + serverMessage);

						Thread.Sleep(1);
					}
					Thread.Sleep(1);
				}
				Thread.Sleep(1);
			}
		} catch (Exception e) {
			Debug.Log("Socket exception: " + e.ToString());
		}
	}
	/// <summary> 	
	/// Send message to server using socket connection. 	
	/// </summary> 	
	private void SendMessage(String str) {
		if (socketConnection==null) { //If connection is problem or missing.
			Debug.Log("SocketConnection problem! Try to reconnecting...");
			return;
		}
		try {
			// Get a stream object for writing. 		
			if (socketConnection.Connected) {
				NetworkStream stream = socketConnection.GetStream();
				if (stream.CanWrite) {
					// Convert string message to byte array.                 
					byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(str);
					// Write byte array to socketConnection stream.                 
					stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
					Debug.Log("Sent message -> " + str);
				}
			}
		} catch (SocketException socketException) {
			Debug.Log("Socket exception: " + socketException);
		}
	}

	void StopClient() {
        if (clientReceiveThread != null) {
			clientReceiveThread.Abort(); 
		}

		if (socketConnection != null) {
			if (socketConnection.Connected) socketConnection.Close();
			socketConnection = null;
		}
	}

	void AddLog(string str) {
		logs = str;
		Debug.Log(str);
	}

    #region All debug log will catch in here.
    void OnEnable() {
		Application.logMessageReceivedThreaded += HandleLog;
	}

	void OnDisable() {
		Application.logMessageReceivedThreaded -= HandleLog;
	}

	void HandleLog(string logString, string stackTrace, LogType type) {
		//if (extension) extension.AppendLog(logString);
		AddLog(logString);
	}
    #endregion

}
