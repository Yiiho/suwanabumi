using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocketTemplate : MonoBehaviour {
    //Dispatcher.
    public delegate void EventHandle(string str);
    public event EventHandle dispatcher;

    [Tooltip("IP or Webaddress to bind.")]
    public string ipAddress;
    [Tooltip("Port to bind.")]
    public int port;
    [Tooltip("Auto bind server after this thread is active.")]
    public bool connectAfterAwake;

    protected virtual void Start() {
        Application.runInBackground = true;
    }
}
