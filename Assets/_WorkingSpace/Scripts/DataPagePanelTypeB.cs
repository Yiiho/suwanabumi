using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DataPagePanelTypeB : MonoBehaviour {
    public TextSender tSocSender;
    public DataPageManager pageManager;
    public Image headerImg;
    public ImageGrouping headerGroup;
    public Image dataImg;
    public ImageGrouping dataGroup;

    public Image[] togglerButtons;
    public Image togglerData;
    public ButtonGroupToggleMapping[] toggleMapping;
    int currentToggler = -1;
    Global global;

    void Start() {
        global = Global.ins;
        headerImg.color = new Color(headerImg.color.r, headerImg.color.g, headerImg.color.b, 0);
        headerImg.sprite = headerGroup.imgData[global.GetLanguageID()];
        headerImg.DOFade(1f, .4f);
        dataImg.color = new Color(dataImg.color.r, dataImg.color.g, dataImg.color.b, 0);
        dataImg.sprite = dataGroup.imgData[global.GetLanguageID()];
        dataImg.DOFade(1f, .4f);
        SetToggler(0);

    }

    void SetToggler(int id) {
        if (id == currentToggler) return;

        togglerData.DOFillAmount(0, .3f).OnComplete(() => {
            togglerData.sprite = toggleMapping[id].imgData[global.GetLanguageID()];
            togglerData.DOFillAmount(1, .5f);
        });
        for(int i = 0; i < toggleMapping.Length; i++) {
            if (i == id) {
                togglerButtons[i].sprite = toggleMapping[i].activeImg[global.GetLanguageID()];
            } else {
                togglerButtons[i].sprite = toggleMapping[i].deactiveImg[global.GetLanguageID()];
            }
        }

        currentToggler = id;
    }

    public void OnPress(int id) {
        SetToggler(id);
    }

    void OnEnable() {
        if (tSocSender) tSocSender.SendString();
    }
}

[System.Serializable]
public struct ButtonGroupToggleMapping {
    public Sprite[] deactiveImg;
    public Sprite[] activeImg;
    public Sprite[] imgData;
}