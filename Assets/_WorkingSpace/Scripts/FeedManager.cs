﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FeedManager : MonoBehaviour {
    public TextSender tSocSender;
    public GameObject thisObj;
    public GameObject nextObj;
    public Transform[] allBullet;
    public Transform activeBullet;
    public FeedMapping[] feeds;
    public Image baseImg;
    public Image newImg;
    public Image dataImg;
    public Image lineImg;
    public Image conditionImg;
    public ImageGrouping conditionGroup;
    public float timerUpdateFeed = 3.0f;

    public float swipeThreshold = 50f;

    int currentIndex;
    Global global;
    Vector2 originPos;
    bool isMoving = false;

    private IEnumerator coroutine;
    enum Direction { Left, Right }

    void Start() {
        global = Global.ins;
        tSocSender.SendString();
        originPos = newImg.transform.position;
        currentIndex = 0;

        conditionImg.sprite = conditionGroup.imgData[global.GetLanguageID()];
        conditionImg.SetNativeSize();
        dataImg.sprite = feeds[currentIndex].imgData[global.GetLanguageID()];

        coroutine = UpdateNewFeed(timerUpdateFeed);
        StartCoroutine(coroutine);
    }

    IEnumerator UpdateNewFeed(float tt) {
        yield return new WaitForSeconds(tt);        
        ShowNewFeed();

        coroutine = UpdateNewFeed(timerUpdateFeed);
        StartCoroutine(coroutine);
    }

    void ShowNewFeed(Direction dirt=Direction.Right) {
        isMoving = true;
        if (dirt == Direction.Right) {
            currentIndex++;
            if (currentIndex >= feeds.Length) currentIndex = 0;            
        } else {
            currentIndex--;
            if (currentIndex < 0) currentIndex = feeds.Length - 1;
            newImg.transform.localPosition = new Vector2(-1920, 0);
        }
        activeBullet.position = allBullet[currentIndex].position;
        newImg.sprite = feeds[currentIndex].bg;
        newImg.transform.DOLocalMoveX(0, .5f).OnComplete(OnMoveEnded);
    }

    void OnMoveEnded() {
        baseImg.sprite = newImg.sprite;
        //dataImg.sprite = feeds[currentIndex].imgData[0];
        dataImg.sprite = feeds[currentIndex].imgData[global.GetLanguageID()];
        dataImg.SetNativeSize();
        dataImg.color = new Color(dataImg.color.r, dataImg.color.g, dataImg.color.g, 0);
        lineImg.color = new Color(lineImg.color.r, lineImg.color.g, lineImg.color.g, 0);
        dataImg.DOFade(1, .3f);
        lineImg.DOFade(1, .3f);
        newImg.transform.position = originPos;
        isMoving = false;
    }

    public void OnTouchPanel() {
        thisObj.transform.DOMoveY(2000, 1).OnComplete( ()=> thisObj.SetActive(false));
        nextObj.SetActive(true);
    }

    float pressPosition;
    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            pressPosition = Input.mousePosition.x;
        } else if (Input.GetMouseButtonUp(0)) {
            OnSwipe(pressPosition, Input.mousePosition.x);
        }
    }

    void OnSwipe(float initX, float finalX) {
        if (isMoving) return; //Can not interrupt while tweening. 
        if(Mathf.Abs(initX - finalX) > swipeThreshold) { //Check user want to swipe or press.
            StopCoroutine(coroutine);
            if(initX - finalX < 0) {
                ShowNewFeed(Direction.Left);
            } else {
                ShowNewFeed(Direction.Right);
            }
            coroutine = UpdateNewFeed(timerUpdateFeed);
            StartCoroutine(coroutine);
        } else {
            OnTouchPanel();
        }
    }

}

[System.Serializable]
public struct FeedMapping {
    public Sprite bg;
    public Sprite[] imgData;
}
