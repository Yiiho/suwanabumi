using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonDataMapping : MonoBehaviour {
    public ButtonGroupSendDataChecker[] buttonGroupChecker;
    public MainManager manager;
    Global global;

    void Start() {
        global = Global.ins;
        if (global) {
            for(int i = 0; i < buttonGroupChecker.Length; i++) {
                if (buttonGroupChecker[i].isSendData) {
                    Button[] allButton = buttonGroupChecker[i].buttonGroup.GetComponentsInChildren<Button>();
                   // print("length button all > " + i + "  <<>> " + allButton.Length);
                    if (allButton.Length > 0) {
                        for (int j = 0; j < allButton.Length; j++) {
                            string val = global.jsonReader.listGroupData[i].keys[j];
                            allButton[j].onClick.RemoveAllListeners();
                            allButton[j].onClick.AddListener(() =>
                                manager.SendDataToMessenger(val));
                        }
                    }
                }
            }
        }
    }
}

[System.Serializable]
public struct ButtonGroupSendDataChecker {
    [Tooltip("Group of each button.")]
    public GameObject buttonGroup;
    [Tooltip("Check it if you want to send socket data.")]
    public bool isSendData;
}
