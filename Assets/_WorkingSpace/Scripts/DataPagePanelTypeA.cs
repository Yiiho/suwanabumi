using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DataPagePanelTypeA : MonoBehaviour {
    public TextSender tSocSender;
    public DataPageManager pageManager;
    public Image headerImg;
    public ImageGrouping headerGroup;
    public Image dataImg;
    public ImageGrouping dataGroup;
    public Image viewImg;
    public Image nextImg;
    public Image backImg;

    float delayButton = .3f;
    Button nextBT, backBT;
    Global global;

    void Start() {
        global = Global.ins;
        headerImg.color = new Color(headerImg.color.r, headerImg.color.g, headerImg.color.b, 0);
        headerImg.sprite = headerGroup.imgData[global.GetLanguageID()];
        headerImg.DOFade(1f, .4f);
        dataImg.color = new Color(dataImg.color.r, dataImg.color.g, dataImg.color.b, 0);
        dataImg.sprite = dataGroup.imgData[global.GetLanguageID()];
        dataImg.DOFade(1f, .4f);

        nextBT = nextImg.GetComponent<Button>();
        backBT = backImg.GetComponent<Button>();
        nextBT.enabled = backBT.enabled = false;
        nextImg.sprite = pageManager.nextButtonGroup.imgData[global.GetLanguageID()];
        backImg.sprite = pageManager.backButtonGroup.imgData[global.GetLanguageID()];
    }

    public void OnPress(int id) {
        pageManager.FadeHomeButton();
        if (id == 0) {
            pageManager.EnablePanelByIndex(pageManager.GetActiveIndex() - 1);
        } else {
            pageManager.EnablePanelByIndex(pageManager.GetActiveIndex() + 1);
        }
    }

    void OnEnable() {
        if (tSocSender) tSocSender.SendString();

        if (nextBT != null) nextBT.enabled = backBT.enabled = false;
        nextImg.color = new Color(nextImg.color.r, nextImg.color.g, nextImg.color.b, 0);
        backImg.color = new Color(backImg.color.r, backImg.color.g, backImg.color.b, 0);
        nextImg.DOFade(1f, .3f).SetDelay(delayButton).OnComplete(() => nextBT.enabled = true);
        backImg.DOFade(1f, .3f).SetDelay(delayButton).OnComplete(() => backBT.enabled = true);
    }
}
