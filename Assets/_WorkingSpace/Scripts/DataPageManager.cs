using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DataPageManager : MonoBehaviour {
    public GameObject[] panelEnable;
    public Image leftPanel;
    public ImageGrouping leftPanelGroup;

    public ImageGrouping backButtonGroup;
    public ImageGrouping nextButtonGroup;
    public Button homeButton;
    int currentPanelEnable = 0;
    float delayButton = .3f;

    void Start() {
       // leftPanel.sprite = leftPanelGroup.imgData[1]; //test
        leftPanel.sprite = leftPanelGroup.imgData[Global.ins.GetLanguageID()];
        EnablePanelByIndex(0);
    }

    public void EnablePanelByIndex(int id) {
        panelEnable[id].SetActive(true);
        if(id != currentPanelEnable) 
            panelEnable[currentPanelEnable].SetActive(false);

        currentPanelEnable = id;
    }

    public void GoHome() {
        if(currentPanelEnable == 0) {
            Global.ins.ChangeScene(1);
        } else {
            Global.ins.ChangeScene(2);
        }
    }

    public int GetActiveIndex() => currentPanelEnable;

    public void FadeHomeButton() {
        print("Fade home button");
        homeButton.enabled = false;
        homeButton.image.color = new Color(homeButton.image.color.r, homeButton.image.color.g, homeButton.image.color.b, 0);
        homeButton.image.DOFade(1f, .3f).SetDelay(delayButton).OnComplete(() => homeButton.enabled = true);
    }
}
