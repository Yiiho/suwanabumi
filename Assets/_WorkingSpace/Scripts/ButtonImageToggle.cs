using UnityEngine;

public class ButtonImageToggle : MonoBehaviour {
    public int defaultActiveObject = -1;
    [Tooltip("All buttons image you want to active when toggle put in here.")]
    public GameObject[] buttonActiveObjects;

    void Start() {
        if (defaultActiveObject > -1) buttonActiveObjects[defaultActiveObject].SetActive(true);
    }

    
    public void ToggleButton(int id) {
        bool isActive;
        for (int i = 0; i < buttonActiveObjects.Length; i++) {
            isActive = (i == id) ? true: false;
            buttonActiveObjects[i].SetActive(isActive);
        }
    }

    public void ResetToDefault() {
        ToggleButton(defaultActiveObject);
    }

    private void OnDisable() {
        ResetToDefault();
    }
}
