using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour {
    Global global;

    public float delayMsgSndBtn;
    float lastMsgSndPress;
    public float delayChgLngBtn;
    float lastChgLngPress;

    public CustomizeImage[] customizeImages;
    public GameObject dataLayer;
    public Image dataImg;

    private int pageIndex;
    void Start() {
        global = Global.ins;
        //search all type objects with non and active.
        customizeImages = Resources.FindObjectsOfTypeAll<CustomizeImage>();

        //DOVirtual.DelayedCall(3f, ()=> dataImg.sprite = global.jsonReader.listGroupData[0].tTH);

        EnablePage(false);

        pageIndex = 0;
      //  dataImg.sprite = global.jsonReader.listGroupData[pageIndex].tTH;
    }
   
    public void OnMessageSender(string s) {
        if (!IsButtonReady("MsgSnd")) return; //cooldown button.
        switch (s) {
            case "A":
                SetPageIndex(0);
                EnablePage();
                break;
            case "B":
                SetPageIndex(1);
                EnablePage();
                break;
            case "C":
                SetPageIndex(2);
                EnablePage();
                break;
            case "D":
                SetPageIndex(3);
                EnablePage();
                break;
            case "0":
                EnablePage(false);
                break;
        }

   //     global.SerialMsgSndr(s);
    }

    private void EnablePage(bool isShow=true) {
        dataLayer.SetActive(isShow);
    }

    private void SetPageIndex(int page) {
        pageIndex = page;
        /*
        if (global.CurrentLanguage == Global.lang.TH) {
            dataImg.sprite = global.jsonReader.listGroupData[pageIndex].tTH;
        } else if (global.CurrentLanguage == Global.lang.EN) {
            dataImg.sprite = global.jsonReader.listGroupData[pageIndex].tEN;
        } else if (global.CurrentLanguage == Global.lang.CN) {
            dataImg.sprite = global.jsonReader.listGroupData[pageIndex].tCN;
        }
        */
    }

    public void OnChangeLang(string s) {
        if (!IsButtonReady("ChgLng")) return; //cooldown button.
        switch (s.ToUpper()) {
            case "TH":
                global.CurrentLanguage = Global.lang.TH;
                break;
            case "EN":
                global.CurrentLanguage = Global.lang.EN;
                break;
            case "CN":
                global.CurrentLanguage = Global.lang.CN;
                break;
            default:
                Debug.LogError("Change language param is mismatch.");
                break;
        }

        print(global.CurrentLanguage);
    }

    bool IsButtonReady(string btType) {
        if (btType == "MsgSnd") {
            if(Time.unscaledTime > lastMsgSndPress + delayMsgSndBtn){
                lastMsgSndPress = Time.unscaledTime;
                return true;
            } 
        }else if (btType == "ChgLng") {
            if (Time.unscaledTime > lastChgLngPress + delayChgLngBtn){
                lastChgLngPress = Time.unscaledTime;
                return true;
            }
        }
        Debug.Log("Have delay.");
        return false;
    }

}
