using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class Global : MonoBehaviour
{
    public static Global ins;
    public JsonReader jsonReader;
    public float maxInterval;
    public SocketServerClient socket;
    public enum lang { TH, EN, CN }

    void Awake() {
        if (ins == null) ins = this;
    }

    void Start() {
        DontDestroyOnLoad(this.gameObject);
        IsStillUpdateInterval = false;
        CurrentLanguage = lang.TH; //Set default language.
        LoadAndContainData();

        Cursor.visible = false;
#if UNITY_EDITOR
        Cursor.visible = true;
#endif
    }

    void LoadAndContainData() {
        jsonReader.OnJsonReaded += OnDataLoaded;
        jsonReader.ReadAllData();
    }

    public void OnDataLoaded() {
        maxInterval = jsonReader.intervalTimer;
        socket.StartConnectToServer(jsonReader.ip, jsonReader.port);

        DOVirtual.DelayedCall(3, () => ChangeScene(1));
    }

    public void ChangeScene(int id) {
        IsStillUpdateInterval = (id == 1) ? false: true; //If this scene is screen saver not update interval.
        SceneManager.LoadScene(id);
    }

    public lang CurrentLanguage {
        get; set;
    }

    public int GetLanguageID() {
        switch (CurrentLanguage) {
            case lang.TH:
                return 0;
            case lang.EN:
                return 1;
            case lang.CN:
                return 2;
        }
        return 0;
    }

    float intervalCount = 0;
    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            intervalCount = 0;
        }
        if (IsStillUpdateInterval) {
            intervalCount += Time.deltaTime;
            if (intervalCount > maxInterval) {
                intervalCount = 0;
                ChangeScene(1);
            }
        }

        if (Input.GetKeyUp(KeyCode.M)) {
            Cursor.visible = !Cursor.visible;
        }
    }

    public bool IsStillUpdateInterval { get; set; }

}


[System.Serializable]
public struct ImageGrouping {
    public Sprite[] imgData;
}
