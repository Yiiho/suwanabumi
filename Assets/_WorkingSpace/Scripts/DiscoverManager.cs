using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DiscoverManager : MonoBehaviour {
    public GameObject outObj;
    public Image headerImg;
    public ImageGrouping headerGroup;
    public Image buttonImg;
    public ImageGrouping buttonGroup;

    void Start() {
        headerImg.sprite = headerGroup.imgData[Global.ins.GetLanguageID()];
        headerImg.SetNativeSize();
        buttonImg.sprite = buttonGroup.imgData[Global.ins.GetLanguageID()];
    }
    
    public void LetDig() {
        outObj.SetActive(true);
        DOVirtual.DelayedCall(3, ()=> Global.ins.ChangeScene(2));
    }

}
